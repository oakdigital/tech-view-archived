<%@page contentType="text/html"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>Tech View - Error</title>
    </head>
    <body>
        <table width='768' height='100%' align='center'>
            <tr>
                <td align='center'>
                    <table width='768' height='412' border='0'>
                        <tr>
                            <td height='24' width='128' align='left' nowrap>
                                <span>Tech View</span>
                            </td>
                            <td height='24' width='384' colspan='3' align='left'>
                            </td>
                            <td height='24' width='128' align='center'>
                            </td>
                            <td height='24' width='128' align='center'>
                            </td>
                        </tr>
                        <tr>
                            <td width='768' height='91' colspan='6' align='left' style='vertical-align:top'>
                            </td>
                        </tr>
                        <tr>
                            <td width='768' height='272' colspan='6' align='center' style='vertical-align:top'>
                                <br>
                                <br>
                                <h1>Error</h1><br>
                                Please send an email to <a href='mailto:webmaster@tech-view.com'>webmaster@tech-view.com</a> to report this bug<br>
                                <br>
                                <b>Tecnical information: </b><br>
                                Exception: <%= pageContext.getException().toString()%><br>
                                Stack Trace: <br>
                                <% pageContext.getException().printStackTrace(new java.io.PrintWriter(out));
            pageContext.getException().printStackTrace();
            Throwable rootCause = null;
            rootCause = pageContext.getException().getCause();
            while (rootCause != null && rootCause.getCause() != null) {
                rootCause = rootCause.getCause();
            }
            if (rootCause != null) {
                                %>
                                Root Cause: <%=rootCause.toString()%><br>
                                Stack Trace: <br>
                                <%
                rootCause.printStackTrace(new java.io.PrintWriter(out));
                rootCause.printStackTrace();
            }
                                %><br>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td height='24' width='128' align='center'>
                            </td>
                            <td height='24' width='128' align='center'>
                            </td>
                            <td height='24' width='128' align='center'>
                            </td>
                            <td height='24' width='384' colspan='3'  align='right'>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <jsp:include page='analyticsTracker.jsp'/>
    </body>
</html>