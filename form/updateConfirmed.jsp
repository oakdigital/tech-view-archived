<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>TECH VIEW</title>
<link rel="stylesheet" href="http://www.tech-view.com/styles/globalheader.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="http://www.tech-view.com/styles/skins/flex_productbrowser.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="http://www.tech-view.com/styles/skins/flex_menu.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="http://www.tech-view.com/styles/skins/beige_light.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="http://www.tech-view.com/styles/text.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="http://www.tech-view.com/styles/form.css" type="text/css" charset="utf-8" />
<!-- searchfield start      searchfield start      searchfield start      searchfield start  -->
<script language="javascript" type="text/javascript" src="http://www.tech-view.com/search/search_jquery.js"></script>
<script language="javascript" type="text/javascript" src="http://www.tech-view.com/search/swap.js"></script>
<!-- searchfield end    searchfield end    searchfield end    searchfield end    searchfield end -->
<script language="JavaScript">
            if (window.XMLHttpRequest) {
                if(document.all){
                    document.write("<link REL='stylesheet' HREF='http://www.tech-view.com/styles/beige_light_netscape.css' TYPE='text/css'>");
                    //IE7
                }else{
                    document.write("<link REL='stylesheet' HREF='http://www.tech-view.com/styles/beige_light_netscape.css' TYPE='text/css'>");
                    //mozilla, safari, opera 9…etc
                }
            } else {
                document.write("<link REL='stylesheet' HREF='http://www.tech-view.com/styles/ie.css' TYPE='text/css'>");
                // IE6, older browsers
            }
        </script>
</head>
<body>
<div id="background_ident">
	<div class="header light">
		<div style="width: 760px; float:left; ">
			<div style="float:right; padding-top:5px;"> <a href="http://www.tech-view.com/dk/news.html">Nyheder</a>&nbsp;&nbsp;&nbsp;I&nbsp;&nbsp;&nbsp;<a href="http://www.tech-view.com/dk/about.html">Om os</a> </div>
		</div>
		<div id="search_box">
			<!-- Google CSE Search Box Begins  -->
			<form action="http://www.tech-view.com/dk/search_results.html" id="cse-search-box"  >
				<input type="hidden" name="cx" value="009884998799720104796:fybrp44fnvg" />
				<input type="hidden" name="cof" value="FORID:11" />
				<input style="height:15px" type="text" name="q" id="s" value="S&oslash;g" class="swap_value" />
				<input type="image" src="http://www.tech-view.com/images/elements/searchfield_light.gif" width="15" height="15" id="go" alt="S&oslash;g" title="S&oslash;g" name="sa" value="S&oslash;g" />
			</form>
			<!-- Google CSE Search Box Ends -->
		</div>
	</div>
	<div id="globalheader_background"> </div>
	<div id="globalheader" class="state-contact">
		<ul id="globalnav">
			<li id="blank"><a href="http://www.tech-view.com/lumaline/index.html">LUMALINE</a></li>
			<li id="blank"><a href="http://www.tech-view.com/flex-display/index.html">FLEX-display</a></li>
			<li id="blank"><a href="http://www.tech-view.com/flag-extender/index.html">FLAG-extender</a></li>
			<li id="blank"><a href="http://www.tech-view.com/download/index.html">Artwork</a></li>
			
			<li id="blank"><a href="http://tech-view.com/news.html">News</a></li>
			<li id="blank"><a href="http://www.tech-view.com/contact/index.html" id="selected">Contact</a></li>
			<li id="company"><a href="http://www.tech-view.com/index.html">TECH VIEW<span style="font-size:9px; vertical-align:text-top;">+</span></a></li>
		</ul>
	</div>
	<div class="mainwindow">
		<div class="column first">
			<div class="menu" id="border_light">
				<ul>
					<li class="level1"><a href="http://www.tech-view.com/contact/index.html">Contact</a></li>
				</ul>
			</div>
		</div>
		<div class="column">
			<!-- CONTENT START      CONTENT START      CONTENT START      CONTENT START      CONTENT START      CONTENT START      CONTENT START      CONTENT START      CONTENT START      CONTENT START -->
			<div id="container770" class="layout2col_right">
				<div class="column">
					<div class="poster h300" id="border_light">
						<div id="header_light">Thank you for your request.</div>
						<div>
							<p>The changes will be applied as soon as possible.</p>
							<p>Kind regards</p>
							<br/>
							<p>The Tech View sales team</p>
						</div>
					</div>
				</div>
				<div class="column last">
					<div class="poster h300" id="border_light">
						<div id="header_light"> Address and contact info </div>
						<div>
							<p>Toldbodgade 51C, 3<span class="style3">rd</span> floor<br />
								DK-1253 Copenhagen K<br />
								Denmark<br />
								<br />
								T: +45 33 12 15 00<br />
								F: +45 33 12 15 08<br />
								<br />
								<a href="mailto:info@tech-view.com">E: info@tech-view.com</a></p>
							<br />
							<br />
							<br />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer light">
			<div style="text-align:right; width: 960px; float:right;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Copyright 2008 &copy; Tech View A/S </div>
		</div>
	</div>
</div>
<jsp:include page='analyticsTracker.jsp'/>
</body>
</html>
