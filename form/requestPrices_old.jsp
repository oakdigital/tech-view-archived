<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%
            com.techview.website.CampaignMail mail = new com.techview.website.CampaignMail();
            boolean pageValid = true;

            if (request.getParameter("company") != null) {
                mail.setCompany(request.getParameter("company"));
            }
            if (request.getParameter("email") != null) {
                mail.setEmail(request.getParameter("email"));
            }
            if (request.getParameter("twinbasePrice") != null) {
                mail.setTwinbasePrice(request.getParameter("twinbasePrice"));
            }
            if (request.getParameter("megaPrice") != null) {
                mail.setMegaPrice(request.getParameter("megaPrice"));
            }
            if (request.getParameter("wallPrice") != null) {
                mail.setWallPrice(request.getParameter("wallPrice"));
            }
            boolean first = true;
            if (request.getParameter("first") != null) {
                first = false;
            }
            boolean companyOk = true;
            boolean emailOk = true;

            if (!first) {
                companyOk = (!mail.getCompany().trim().equals(""));
                emailOk = (!mail.getEmail().trim().equals(""));
                pageValid = (companyOk && emailOk);

                if (pageValid) {
                    mail.sendRequestMail();
                    // Send mail
                    response.sendRedirect("pricesConfirmed.jsp");
                    // Redirect to ok page
                }
            }
%>

<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/TR/REC-html40'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
<title>FLEX-display XL, MEGA and WALL</title>
<script language="Javascript" SRC="scripts/ColorPicker2.js"></script>
    <script language="Javascript">
        var cp = new ColorPicker('window'); // Popup window
        var cp2 = new ColorPicker(); // DIV style
    </script>
</head>
<body bgcolor='#e8e8e8' marginheight='0px' marginwidth='0px'>
<form action='requestPrices.jsp' method='POST' >
<table style='width:600px; height:50px; background-image:url(images/BAR_GRADIENT.jpg)' align='center' width='600px' height='50px' cellpadding='0' cellspacing='0' border='0px' background='images/BAR_GRADIENT.jpg' bgcolor='#FF3300'>
	<tr>
		<td height='50px' valign='top'>
			<table style='width:600px; height:50px;' align='center' width='600px' height='50px' cellpadding='0' cellspacing='0' border='0px'>
            <tr>

                <td width='10px' valign='top'>&nbsp;</td>
                <td height='50px' valign='middle'><span style='font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; color:#FFFFFF; font-size: 18px;'>FLEX-display Twin base systems</span></td>
                <td align='right' style='text-align:right;'><span style='font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; color:#FFFFFF; font-size: 18px;'><strong>TECH VIEW&trade;</strong></span></td>
                <td width='10px'>&nbsp;</td>
            </tr>
            </table>
        </td>
    </tr>
</table>
		
<table bgcolor='#FFFFFF' style='width:600px;' align='center' width='600px' cellpadding='5' cellspacing='0'>
	<tr>
		<td colspan='2'> <br />
			<h1 style='font-size:18px;font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-weight:bold; padding:0px; padding-right:10px; padding-left:10px; margin:0px;'> Large walls - <strong>small prices</strong></h1>
			<p style='padding:0px; padding-right:10px; padding-left:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'>Once you have typed in your company name, email, prices, color and press on "Send mail to me" we will send an email to the specified email adresse - from here you can forward it as a regular newsletter.</p>
			<br />

			<br />
		</td>
	</tr>
    <tr>
        <td align="left" >
        <p><font face="Trebuchet MS">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;<A HREF="#" onClick="cp.select(document.forms[0].headerColor,'pick');return false;" NAME="pick" ID="pick">Header color</A></font>
        <INPUT TYPE="text" NAME="headerColor" VALUE='<%=mail.getHeaderColor()%>'>
        <script language="JavaScript">cp.writeDiv()</script>
        </td>
    </tr>
    <tr>
        <td align="center" >
            <p <% if (!companyOk) {
                            out.print("id='selected'");}%>><font face="Trebuchet MS">Company <span id="selected">*</span></font>
            <input type='text' name='company' class='standard' value='<%=mail.getCompany()%>' /></p>
        </td>
        <td align="left" >
            <p <% if (!emailOk) {
                            out.print("id='selected'");}%>><font face="Trebuchet MS">E-mail <span id="selected">*</span></font>
            <input type='text' name='email' class='standard' value='<%=mail.getEmail()%>' /></p>
        </td>
    </tr>
	<tr>
		<td width='330'><img src='http://media.portable-expo.pro/images/Campaign/xl.png' alt='Twin Base XL' width='331' height='273' hspace='0' vspace='0' border='0' />
			<p>&nbsp;</p>
		</td>
		<td valign='top'>
			<h1 style='font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; padding:0px; margin:0px;'>Twin Base XL</h1>
			<p style='padding:0px; margin:0px; padding-right:10px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'><span style='padding:0px; padding-right:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'>FIll in your retail price for the XL system</span></p>
            <p style='padding:0px; margin:0px; padding-right:10px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:16px; color:#99FF00;'><b>Price EUR </b><input type='text' name='twinbasePrice' class='standard' value='<%=mail.getTwinbasePrice()%>' /></p>
			<p style='padding:0px; margin:0px; padding-right:10px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'> Price per unit at 20 units.<br />
				Incl. banner. &nbsp Excl. bag and freight </p>
		</td>
	</tr>
	<tr>
		<td> <img src='http://media.portable-expo.pro/images/Campaign/mega.png' alt='Twin Base MEGA' width='331' height='273' hspace='0' vspace='0' border='0' />
			<p>&nbsp;</p>
		</td>
		<td valign='top'>
			<h1 style='font-size:18px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-weight:bold; padding:0px; margin:0px;'><span style='font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; padding:0px; margin:0px;'>Twin Base MEGA</span></h1>
			<p style='padding:0px; padding-right:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'>FIll in your retail price for the MEGA system</p>

			<p style='padding:0px; margin:0px; padding-right:10px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:16px; color:#99FF00;'><b>Price EUR </b><input type='text' name='megaPrice' class='standard' value='<%=mail.getMegaPrice()%>' /> </p>
			<p style='padding:0px; padding-right:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'> Price per unit at 10 units.<br />
				Incl. banner. &nbsp Excl. bag and freight </p>
		</td>
	</tr>
	<tr>

		<td><img src='http://media.portable-expo.pro/images/Campaign/wall.png' alt='Twin Base WALL' width='331' height='273' hspace='0' vspace='0' border='0' />
			<p>&nbsp;</p>
		</td>
		<td valign='top'>
			<h1 style='font-size:18px;font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-weight:bold; padding:0px; padding-right:10px; padding-left:10px; margin:0px;'><span style='font-size:18px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-weight:bold; padding:0px; margin:0px;'><span style='font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; padding:0px; margin:0px;'>Twin Base WALL</span></span></h1>
			<p style='padding:0px; padding-right:10px; padding-left:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'><span style='padding:0px; padding-right:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'>FIll in your retail price for the WALL system</span></p>
            <p style='padding:0px; margin:0px; padding-right:10px; padding-left:10px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:16px; color:#99FF00;'><b>Price EUR </b><input type='text' name='wallPrice' class='standard' value='<%=mail.getWallPrice()%>' /> </p>

			<p style='padding:0px; padding-right:10px; padding-left:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'> Price per unit at 5 units.<br />
				Incl. banner. &nbsp Excl. bag and freight </p>
		</td>
	</tr>
</table>
<p align="center">
    <input align="center"  type="submit" name='first' value='Send mail to me' />
</p>
</form>
<table align='center' width='600px' height='50px' cellpadding='0' cellspacing='0'>
	<tr>

		<td align='center' style='border-top: 1px solid #000;'> <img src='http://media.portable-expo.pro/images/Campaign/splash.gif' alt='services' width='577' height='98' hspace='0' vspace='0' border='0' /> <br />
		</td>
	</tr>
</table>
<div align="center"><font size="1" face="Trebuchet MS"><br>
        TECH VIEW A/S | Tel:
        + 45 33 12 15 00 | Fax: + 45 33 12 15 08<br>
    <a href="mailto:info@tech-view.com">E-mail:info@tech-view.com</a> | <a href="http://www.tech-view.com">http://www.tech-view.com</a></font>
</div>
<jsp:include page='analyticsTracker.jsp'/>
</body>
</html>







