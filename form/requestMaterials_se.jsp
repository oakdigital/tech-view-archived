<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%
            com.techview.website.RetailerMail mail = new com.techview.website.RetailerMail();
            boolean pageValid = true;

            if (request.getParameter("name2") != null) {
                mail.setName2(request.getParameter("name2"));
            }
            if (request.getParameter("email2") != null) {
                mail.setEmail2(request.getParameter("email2"));
            }
            if (request.getParameter("lumalineBrochurer") != null) {
                mail.setLumalineBrocurer(request.getParameter("lumalineBrochurer") != null);
            }
            if (request.getParameter("lumalinePrisliste") != null) {
                mail.setLumalinePrisliste(request.getParameter("lumalinePrisliste") != null);
            }
            if (request.getParameter("flexdisplayBrochurer") != null) {
                mail.setFlexdisplayBrocurer(request.getParameter("flexdisplayBrochurer") != null);
            }
            if (request.getParameter("flexdisplayPrisliste") != null) {
                mail.setFlexdisplayPrisliste(request.getParameter("flexdisplayPrisliste") != null);
            }
            if (request.getParameter("ekstraMaterials") != null) {
                mail.setEkstraMaterials(request.getParameter("ekstraMaterials") != null);
            }
            if (request.getParameter("other") != null) {
                mail.setOther(request.getParameter("other"));
            }
            boolean first = true;
            if (request.getParameter("first") != null) {
                first = false;
            }
            boolean name2Ok = true;
            boolean company2Ok = true;
            boolean email2Ok = true;

            if (!first) {
                name2Ok = (!mail.getName2().trim().equals(""));
                email2Ok = (!mail.getEmail2().trim().equals(""));
                pageValid = (name2Ok && email2Ok);

                if (pageValid) {

                    mail.setClientIp(request.getRemoteAddr());
                    mail.setClientHost(request.getRemoteHost());
                    mail.sendRequestMail();

// Send mail
                    response.sendRedirect("requestConfirmed_se.jsp");
// Redirect to ok page
                }
            }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title>TECH VIEW</title>
    </head>
    <body bgcolor="#FFFFFF" text="#000000" link="#333333" vlink="#333333" alink="#333333" leftmargin="0" topmargin="8" marginwidth="0" marginheight="0">
        <form action='requestMaterials_se.jsp' method='POST' >
            <table width="600" height="493" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
                <tr>
                    <td align="center" valign="top"> <img name="top" src="http://media.tech-view.com/Campaign/3D_presentation_08/images/top.gif" width="600" height="36" border="0" alt=""> 
                        <table width="600" border="0" cellpadding="10" cellspacing="0" bgcolor="#FFFFFF">
                            <tr> 
                                <td><table width="502" border="0" align="center">
                                        <tr>
                                            <td align="left"><p>
                                                <font face="Trebuchet MS" size="3">Bocka gärna för här nedan och beställ ytterligare material från oss helt <strong>kostnadsfritt</strong>.</font></p>
                                                <table width="482" border="0" cellpadding="5" cellspacing="0" bordercolor="#999999">
                                                    <tr align="left">
                                                        <td align="left" valign="top">
                                                            <div align="left">
                                                            <input class="checkbox" type='checkbox' <%= ((mail.isLumalineBrocurer()) ? "checked" : "")%> name='lumalineBrochurer' />
                                                        </div></td>
                                                        <td><font face="Trebuchet MS" size="3">10 neutrala Lumaline™ broschyrer för er egen marknadsföring.<strong></strong><br>
                                                                <br>
                                                        </font></td>
                                                    </tr>
                                                    <tr align="left">
                                                        <td align="left" valign="top">
                                                            <div align="left">
                                                            <input class="checkbox" type='checkbox' <%= ((mail.isLumalinePrisliste()) ? "checked" : "")%> name='lumalinePrisliste' />
                                                        </div></td>
                                                        <td><font face="Trebuchet MS" size="3">Återförsäljar-prislista för Lumaline™ produkter.<strong></strong><br>
                                                                <br>
                                                        </font></td>
                                                    </tr>
                                                    <tr align="left">
                                                        <td align="left" valign="top">
                                                            <div align="left">
                                                            <input class="checkbox" type='checkbox' <%= ((mail.isFlexdisplayBrocurer()) ? "checked" : "")%> name='flexdisplayBrochurer' />
                                                        </div></td>
                                                        <td><font face="Trebuchet MS" size="3">10 neutrala FLEX-display™ broschyrer för er egen marknadsföring.<strong></strong><br>
                                                                <br>
                                                        </font></td>
                                                    </tr>
                                                    <tr align="left">
                                                        <td align="left" valign="top">
                                                                <div align="left">
                                                                <input class="checkbox" type='checkbox' <%= ((mail.isFlexdisplayPrisliste()) ? "checked" : "")%> name='flexdisplayPrisliste' />
                                                        </div></td>
                                                        <td><font face="Trebuchet MS" size="3">Återförsäljar-prislista för FLEX-display™ produkter.<strong></strong><br>
                                                        </font></td>
                                                    </tr>
                                                    <tr align="left">
                                                        <td align="left" valign="top">&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr align="left">
                                                        <td align="left" valign="top"><div align="left">
                                                                <input class="checkbox" type="checkbox" <%= ((mail.isEkstraMaterials()) ? "checked" : "")%> name="ekstraMaterials" />
                                                        </div></td>
                                                        <td><font face="Trebuchet MS" size="3">Ytterligare marknadsföringsmaterial för era e-postkampanjer eller hemsida.<p></p></font></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan='2' align='left' valign='top'>
                                                            <p>
                                                                <font face='Trebuchet MS' size='3'>
                                                                    Kommentarer:
                                                                    <br>
                                                                    <textarea name='other' cols='52' rows='4' value='<%= mail.getOther()%>' /></textarea>
                                                                </font>
                                                            </p>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
                                                <table width="550" border="0">
                                                    <tr>
                                                        <td width="43"><font face="Trebuchet MS">Namn:</font></td>
                                                        <td width="155">
                                                            <input type='text' name='name2' size='17' value='<%=mail.getName2()%>' />
                                                        </td>
                                                        <td width="16">&nbsp;</td>
                                                        <td width="53"><font face="Trebuchet MS">E-post:</font></td>
                                                        <td width="256">
                                                            <input type='text' name='email2' size='19' value='<%=mail.getEmail2()%>' />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <p align="center">
                                                    <input align="center"  type="submit" name='first' value='Skicka' />
                                                    <br>
                                                </p>
                                            </td>
                                        </tr>
                                    </table>
                                    <hr width="502" />
                                    <table width="502" border="0" align="center">
                                        <tr>
                                            <td><p align="center"><img name="products_signature" src="http://media.tech-view.com/Campaign/3D_presentation_08/images/products_signature.jpg" width="472" height="323" border="0" id="products_signature" alt="Products" /></p>
                                                <p align="center"><font face="Trebuchet MS" size="3">Vi ser fram emot att höra ifrån er!<br>
                                            </font></p></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><div align="center">
                                        <hr width="502" />
                                        <table width="502" border="0" align="center">
                                            <tr>
                                                <td><p align="center"><font face="Trebuchet MS" size="3">Besök gärna vår webbsida för mer inspiration och information<br />
                                                            <a href="http://www.tech-view.com">www.tech-view.com</a><br>
                                                </font></p></td>
                                            </tr>
                                        </table>
                                        <hr width="502" />
                                </div></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </form>
        <div align="center"><font size="1" face="Trebuchet MS">TECH VIEW A/S | Tel: 
                + 45 33 12 15 00 | Fax: + 45 33 12 15 08<br>
            <a href="mailto:info@tech-view.com">E-mail:info@tech-view.com</a> | <a href="http://www.tech-view.com">http://www.tech-view.com</a></font>
            <br />
            <br />
        </div>
        <jsp:include page='analyticsTracker.jsp'/>
    </body>
</html>