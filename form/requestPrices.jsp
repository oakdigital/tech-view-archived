<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%
            com.techview.website.CampaignMail mail = new com.techview.website.CampaignMail();
            boolean pageValid = true;

            if (request.getParameter("company") != null) {
                mail.setCompany(request.getParameter("company"));
            }
            if (request.getParameter("email") != null) {
                mail.setEmail(request.getParameter("email"));
            }
            if (request.getParameter("twinbasePrice") != null) {
                mail.setTwinbasePrice(request.getParameter("twinbasePrice"));
            }
            if (request.getParameter("megaPrice") != null) {
                mail.setMegaPrice(request.getParameter("megaPrice"));
            }
            if (request.getParameter("wallPrice") != null) {
                mail.setWallPrice(request.getParameter("wallPrice"));
            }
            boolean first = true;
            if (request.getParameter("first") != null) {
                first = false;
            }
            boolean companyOk = true;
            boolean emailOk = true;

            if (!first) {
                companyOk = (!mail.getCompany().trim().equals(""));
                emailOk = (!mail.getEmail().trim().equals(""));
                pageValid = (companyOk && emailOk);

                if (pageValid) {
                    mail.sendRequestMail();
                    // Send mail
                    response.sendRedirect("pricesConfirmed.jsp");
                    // Redirect to ok page
                }
            }
%>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/TR/REC-html40'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
<title>FLEX-display XL, MEGA and WALL</title>
<script language="Javascript" SRC="scripts/ColorPicker2.js"></script>
<script language="Javascript">
        var cp = new ColorPicker('window'); // Popup window
        var cp2 = new ColorPicker(); // DIV style
    </script>
</head>
<body bgcolor='#e8e8e8' marginheight='0px' marginwidth='0px'>
<form action='requestPrices.jsp' method='POST' >
	<!-- INSTRUCTIONS FORWARDING: START -->
	<table width="600px" align="center">
		<tr>
			<td>
				<p style="padding:10px; padding-top:0px; padding-bottom:0px; font-size: 16px;font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif;">E-MAIL CAMPAIGN FOR YOUR CUSTOMERS!<br />
					<br />
					Follow these simple steps to recieve a finished mailcampaing in your inbox</p>
				<ol style=" font-size: 12px;font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif;">
					<li>Change the color of the top-bar and fill in <strong>your company name</strong>
						<ul>
							<li>The name will be displayed in the colored bar at the top of the mail.</li>
							<br />
						</ul>
					</li>
					<li>Fill in <strong>your e-mail adresse</strong> and <strong>Change the prices</strong> to include your profit<br />
						<ul>
							<li>If you do not change any prices, the default retailprice will be displayed.</li>
							<li>NOTE: You are free to print the banners yourself. The indicated prices are valid if we supply the banners. <a href="http://media.tech-view.com/Campaign/megaBase/Pricelist.pdf" target="_blank">You can see your hardware pricing, services and accessories HERE</a>.</li>
							<br />
						</ul>
					</li>
					<li> press &quot;send mail to me&quot;<br />
						<ul>
							<li>When you press &quot;send mail to me&quot;, the newsletter will be send to the adresse you have specified.</li>
							<br />
						</ul>
					</li>
				</ol>
				<p style="padding:10px; padding-top:0px; padding-bottom:0px; font-size: 16px;font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif;">From your inbox:</p>
				<ol style=" font-size: 12px;font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif;">
					<li>Once you have recieved the mail in your inbox - <strong>press forward</strong><br />
						<ul>
							<li><img src="http://media.portable-expo.pro/images/Campaign/icons/Billede 4.png" width="67" height="20" align="top" /></li>
							<li>The mailcampaign is now ready.</li>
							<br />
						</ul>
					</li>
					<li><strong>Fill in recipients and press send</strong>
						<ul>
							<li>NOTE: BCC all recipients, that way your clients names are not revealed.</li>
							<li><img src="http://media.portable-expo.pro/images/Campaign/icons/Billede 6.png" width="78" height="17" align="top" /></li>
						</ul>
					</li>
				</ol>
				<p></p>
			</td>
		</tr>
	</table>
	<!-- INSTRUCTIONS FORWARDING: END -->
	<table style='width:600px; height:50px; background-image:url(images/BAR_GRADIENT.jpg)' align='center' width='600px' height='50px' cellpadding='0' cellspacing='0' border='0px' background='images/BAR_GRADIENT.jpg' bgcolor='#FF3300'>
		<tr>
			<td height='50px' valign='top'>
				<table style='width:600px; height:50px;' align='center' width='600px' height='50px' cellpadding='0' cellspacing='0' border='0px'>
					<tr>
						<td width='10px' valign='top'>&nbsp;</td>
						<td height='50px' valign='middle'><span style='font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; color:#FFFFFF; font-size: 18px;'>FLEX-display Twin base systems</span></td>
						<td align='right' style='text-align:right;'><span style='font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; color:#FFFFFF; font-size: 18px;'><strong>TECH VIEW&trade;</strong></span></td>
						<td width='10px'>&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<table bgcolor='#FFFFFF' style='width:600px;' align='center' width='600px' cellpadding='5' cellspacing='0'>
		<tr>
			<td colspan='2'> <br />
				<h1 style='font-size:18px;font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-weight:bold; padding:0px; padding-right:10px; padding-left:10px; margin:0px;'> Large walls - <strong>small prices</strong></h1>
				<p style='padding:0px; padding-right:10px; padding-left:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'>Once you have typed in your company name, e-mail &amp; selected a color for the top bar - fill in your retail prices for the selected systems and press "Send mail to me", we will then submit the modified newsletter in an email to the your e-mail adresse (the e-mail specified).<br>
					<br>
					From here you can forward it as a regular newsletter.<br />
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p style='padding:0px; padding-right:10px; padding-left:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'><a HREF="#" onClick="cp.select(document.forms[0].headerColor,'pick');return false;" NAME="pick" ID="pick">Header color</a>
					<input TYPE="text" NAME="headerColor" VALUE='<%=mail.getHeaderColor()%>' />
					<script language="JavaScript">cp.writeDiv()</script>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p style='padding:0px; padding-right:10px; padding-left:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'<% if (!companyOk) {out.print("id='selected'");}%>>Company <span id="selected">*</span>
					<input type='text' name='company' class='standard' value='<%=mail.getCompany()%>' />
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p style='padding:0px; padding-right:10px; padding-left:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'<% if (!emailOk) {
                            out.print("id='selected'");}%>>E-mail <span id="selected">*</span>
					<input type='text' name='email' class='standard' value='<%=mail.getEmail()%>' />
				</p>
			</td>
		</tr>
		<tr>
		<td width='320'><img src='http://media.portable-expo.pro/images/Campaign/xl.jpg' alt='Twin Base XL' width='328' height='322' hspace='0' vspace='0' border='0' /><br />
			<a href='#'><img src='http://media.portable-expo.pro/images/Campaign/play.gif' width='110' height='19' border='0px' /></a>
			<p>&nbsp;</p>
			</td>
			<td valign='top'>
				<h1 style='font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; padding:0px; margin:0px;'>Twin Base XL</h1>
				<p style='padding:0px; margin:0px; padding-right:10px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'><span style='padding:0px; padding-right:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'>FIll in your retail price for the XL system</span></p>
				<p style='padding:0px; margin:0px; padding-right:10px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:16px; color:#99FF00;'><b>Price EUR </b>
					<input type='text' name='twinbasePrice' class='standard' value='<%=mail.getTwinbasePrice()%>' />
				</p>
				<p style='padding:0px; margin:0px; padding-right:10px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'> Price per unit at 20 units.<br />
					Incl. banner. &nbsp Excl. bag and freight </p>
			</td>
		</tr>
		<tr>
		<td> <img src='http://media.portable-expo.pro/images/Campaign/mega.jpg' alt='Twin Base MEGA' width='328' height='272' hspace='0' vspace='0' border='0' /><br />
			<a href='#'><img src='http://media.portable-expo.pro/images/Campaign/play.gif' width='110' height='19' border='0px' /></a>
			<p>&nbsp;</p>
			</td>
			<td valign='top'>
				<h1 style='font-size:18px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-weight:bold; padding:0px; margin:0px;'><span style='font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; padding:0px; margin:0px;'>Twin Base MEGA</span></h1>
				<p style='padding:0px; padding-right:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'>FIll in your retail price for the MEGA system</p>
				<p style='padding:0px; margin:0px; padding-right:10px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:16px; color:#99FF00;'><b>Price EUR </b>
					<input type='text' name='megaPrice' class='standard' value='<%=mail.getMegaPrice()%>' />
				</p>
				<p style='padding:0px; padding-right:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'> Price per unit at 10 units.<br />
					Incl. banner. &nbsp Excl. bag and freight </p>
			</td>
		</tr>
		<tr>
		<td><img src='http://media.portable-expo.pro/images/Campaign/wall.jpg' alt='Twin Base WALL' width='328' height='272' hspace='0' vspace='0' border='0' /><br />
			<a href='#'><img src='http://media.portable-expo.pro/images/Campaign/play.gif' width='110' height='19' border='0px' /></a>
			<p>&nbsp;</p>
			</td>
			<td valign='top'>
				<h1 style='font-size:18px;font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-weight:bold; padding:0px; padding-right:10px; padding-left:10px; margin:0px;'><span style='font-size:18px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-weight:bold; padding:0px; margin:0px;'><span style='font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:18px; font-weight:bold; padding:0px; margin:0px;'>Twin Base WALL</span></span></h1>
				<p style='padding:0px; padding-right:10px; padding-left:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'><span style='padding:0px; padding-right:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'>FIll in your retail price for the WALL system</span></p>
				<p style='padding:0px; margin:0px; padding-right:10px; padding-left:10px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:16px; color:#99FF00;'><b>Price EUR </b>
					<input type='text' name='wallPrice' class='standard' value='<%=mail.getWallPrice()%>' />
				</p>
				<p style='padding:0px; padding-right:10px; padding-left:10px; margin:0px; font-family: Trebuchet MS, Verdana, Arial, Helvetica, sans-serif; font-size:12px;'> Price per unit at 5 units.<br />
					Incl. banner. &nbsp Excl. bag and freight </p>
			</td>
		</tr>
		<tr>
			<td colspan='2'>
				<p align='center'>
					<input align='center'  type='submit' name='first' value='Send mail to me' />
				</p>
			</td>
		</tr>
	</table>
</form>
<table align='center' width='600px' height='50px' cellpadding='0' cellspacing='0'>
	<tr>
		<td align='center' style='border-top: 1px solid #000;'> <img src='http://media.portable-expo.pro/images/Campaign/splash.gif' alt='services' width='577' height='98' hspace='0' vspace='0' border='0' /> <br />
		</td>
	</tr>
</table>
<div align='center'><font size='1' face='Trebuchet MS'><br />
	TECH VIEW A/S | Tel:
	+ 45 33 12 15 00 | Fax: + 45 33 12 15 08<br />
	<a href='mailto:info@tech-view.com'>E-mail:info@tech-view.com</a> | <a href='http://www.tech-view.com'>http://www.tech-view.com</a></font> </div>
<jsp:include page='analyticsTracker.jsp'/>
</body>
</html>
