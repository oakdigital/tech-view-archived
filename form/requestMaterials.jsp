<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%
            com.techview.website.RetailerMail mail = new com.techview.website.RetailerMail();
            boolean pageValid = true;

            if (request.getParameter("name2") != null) {
                mail.setName2(request.getParameter("name2"));
            }
            if (request.getParameter("company2") != null) {
                mail.setCompany2(request.getParameter("company2"));
            }
            if (request.getParameter("email2") != null) {
                mail.setEmail2(request.getParameter("email2"));
            }
            if (request.getParameter("lumalineBrochurer") != null) {
                mail.setLumalineBrocurer(request.getParameter("lumalineBrochurer") != null);
            }
            if (request.getParameter("lumalinePrisliste") != null) {
                mail.setLumalinePrisliste(request.getParameter("lumalinePrisliste") != null);
            }
            if (request.getParameter("flexdisplayBrochurer") != null) {
                mail.setFlexdisplayBrocurer(request.getParameter("flexdisplayBrochurer") != null);
            }
            if (request.getParameter("flexdisplayPrisliste") != null) {
                mail.setFlexdisplayPrisliste(request.getParameter("flexdisplayPrisliste") != null);
            }
            boolean first = true;
            if (request.getParameter("first") != null) {
                first = false;
            }
            boolean name2Ok = true;
            boolean company2Ok = true;
            boolean email2Ok = true;

            if (!first) {
                name2Ok = (!mail.getName2().trim().equals(""));
                company2Ok = (!mail.getCompany2().trim().equals(""));
                email2Ok = (!mail.getEmail2().trim().equals(""));
                boolean lumalineBrochurerOk = request.getParameter("lumalineBrochurer") != null;
                boolean lumalinePrislisteOk = request.getParameter("lumalinePrisliste") != null;
                boolean flexdisplayBrochurerOk = request.getParameter("flexdisplayBrochurer") != null;
                boolean flexdisplayPrislisteOk = request.getParameter("flexdisplayPrisliste") != null;
                pageValid = (name2Ok && company2Ok && email2Ok);

                if (pageValid) {

                    mail.setClientIp(request.getRemoteAddr());
                    mail.setClientHost(request.getRemoteHost());
                    mail.sendRequestMail();

// Send mail
                    response.sendRedirect("requestConfirmed.jsp");
// Redirect to ok page
                }
            }
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title>TECH VIEW</title>
</head>
<body bgcolor="#333333" text="#FFFFFF" link="#CCCCCC" vlink="#CCCCCC" alink="#CCCCCC" leftmargin="0" topmargin="8" marginwidth="0" marginheight="0">
<form action='requestMaterials.jsp' method='POST' >
<table width="600" height="493" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#3D3F40">
<tr>
<td align="center" valign="top"> <img name="top" src="http://media.tech-view.com/Campaign/3D_presentation_08/images/top.gif" width="600" height="36" border="0" alt=""> 
<table width="600" border="0" cellpadding="10" cellspacing="0" bgcolor="#3d3f40">
<tr> 
<td><table width="502" border="0" align="center">
<tr>
<td><p><font face="Trebuchet MS"><font size="3">The 3D presentations, and many other relevant marketing material is free for you to use in yor own marketing.<br>
<br></font><font face="Trebuchet MS"><font size="3"><font face="Trebuchet MS">Please tick the <font size="3"><font face="Trebuchet MS">request</font></font>ed checkboxes.</font></font></font></font></p>
<table width="500" border="0" cellpadding="0" cellspacing="0" bordercolor="#999999">
    <tr>
        <td width="35" align="left" valign="top">
            <div align="left">
                <input class="checkbox" type='checkbox' <%= ((mail.isLumalineBrocurer()) ? "checked" : "")%> name='lumalineBrochurer' />
        </div></td>
        <td width="459"><font face="Trebuchet MS" size="3">10  neutrale Lumaline&#8482; brochurers for your marketing<br>
                <br>
        </font></td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <div align="left">
                <input class="checkbox" type='checkbox' <%= ((mail.isLumalinePrisliste()) ? "checked" : "")%> name='lumalinePrisliste' />
        </div></td>
        <td><font face="Trebuchet MS" size="3">Dealerpricelists for Lumaline&#8482; products<br>
                <br>
        </font></td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <div align="left">
                <input class="checkbox" type='checkbox' <%= ((mail.isFlexdisplayBrocurer()) ? "checked" : "")%>  name='flexdisplayBrochurer' />
        </div></td>
        <td><font face="Trebuchet MS" size="3">10  neutrale FLEX-display&#8482; brochurers for your marketing<br>
                <br>
        </font></td>
    </tr>
    <tr>
        <td align="left" valign="top">
            <div align="left">
                <input class="checkbox" type='checkbox' <%= ((mail.isFlexdisplayPrisliste()) ? "checked" : "")%>  name='flexdisplayPrisliste' />
        </div></td>
        <td><font face="Trebuchet MS" size="3">Dealerpricelists for FLEX-display&#8482; products<br>
        </font></td>
    </tr>
</table>
<br />
<table>
<tr>
<td class="form_left">
<p style="padding-bottom:0px;" <% if (!name2Ok) {
                out.print("color='selected'");}%>><font face="Trebuchet MS">Name <span id="selected">*</span></font><br />
<input type='text' name='name2' class='standard' value='<%=mail.getName2()%>' /></p>
</td>
<td class="form_right">
<p <% if (!company2Ok) {
                out.print("id='selected'");}%>><font face="Trebuchet MS">Company <span id="selected">*</span></font><br />
<input type='text' name='company2' class='standard' value='<%=mail.getCompany2()%>' /></p>
</td>
<td class="form_right">
<p <% if (!email2Ok) {
                out.print("id='selected'");}%>><font face="Trebuchet MS">E-mail <span id="selected">*</span></font><br />
<input type='text' name='email2' class='standard' value='<%=mail.getEmail2()%>' /></p>
</td>
</tr>
</table>
<p align="center">
    <input align="center"  type="submit" name='first' value='Send' />
</p>
<p align="center"><font size="2" face="Trebuchet MS">* These fields are mandatory</font></p>
<br>
</td>
</tr>
</table>
<table width="502" border="0" align="center">
    <tr>
        <td><p align="center"><font face="Trebuchet MS" size="3">We are looking foreward to hear from you<br>
        </font></p></td>
    </tr>
</table>
<br>
<hr width="502"></td>
</tr>
<tr>
    <td><div align="center">
            <table width="502" border="0" align="center">
                <tr>
                    <td><p align="center"><font face="Trebuchet MS" size="3">Find more information and inspiration from our website<br />
                                <a href="http://www.tech-view.com">www.tech-view.com</a><br>
                    </font></p></td>
                </tr>
            </table>
    </div></td>
</tr>
</table>
</td>
</tr>
</table>
</form>
<div align="center"><font size="1" face="Trebuchet MS"><br>
        TECH VIEW A/S | Tel: 
        + 45 33 12 15 00 | Fax: + 45 33 12 15 08<br>
    <a href="mailto:info@tech-view.com">E-mail:info@tech-view.com</a> | <a href="http://www.tech-view.com">http://www.tech-view.com</a></font>
</div>
<jsp:include page='analyticsTracker.jsp'/>
</body>
</html>