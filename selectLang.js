function redirect(locale) {
	var dest = 'http://www.tech-view.com/index.html';
	var lang = 'en';
	if(locale){
		// Use selected language.
		if(locale == 'da'){
			dest = 'http://www.tech-view.com/dk/index.html';
		} 
		else {
			dest = 'http://www.tech-view.com/index.html';
		}
	}
	else{
		// Use default language
		var langCode = navigator.systemLanguage || navigator.language;
		lang = langCode.toLowerCase(); 
		lang = lang.substr(0,2); 
		if(lang == 'da'){
			dest = 'http://www.tech-view.com/dk/index.html';
		}
		else {
			dest = 'http://www.tech-view.com/index.html';

		}
	}
	// redirect to correct page if not allready there.
	if(window.location != dest){
		window.location = ''+dest;
	}
}

function set_lang( name, value, expires, path, domain, secure ) {
	// set time, it's in milliseconds
	var today = new Date();
	today.setTime( today.getTime() );

	if ( expires ){
		Expires = expires * 1000 * 60 * 60 * 24;
	}
	var expires_date = new Date( today.getTime() + (expires) );

	document.cookie = name + "=" +escape( value ) +
	( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + ( ( path ) ? ";path=" + path : "" ) + 
	( ( domain ) ? ";domain=" + domain : "" ) + ( ( secure ) ? ";secure" : "" );
}

function get_lang( name ) {
	var start = document.cookie.indexOf( name + "=" );
	var len = start + name.length + 1;
	if ( ( !start ) && ( name != document.cookie.substring( 0, name.length ) ) ) {
		return null;
	}
	if ( start == -1 ) return null;
	var end = document.cookie.indexOf( ";", len );
	if ( end == -1 ) end = document.cookie.length;
		return unescape( document.cookie.substring( len, end ) );
}
