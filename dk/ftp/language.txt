# Buttons
mkDirBtn = Mkdir
renameBtn = Rename
deleteBtn = Delete
refreshBtn = Refresh
uploadItem = Upload
downloadItem = Download
uploadBtn = >>
downloadBtn = <<
okBtn = OK
cancelBtn = Cancel
connectBtn = Connect
disconnectBtn = Disconnect
helpBtn = Help
aboutBtn = About
resumeBtn = Resume

# Applet Labels
localSystemLbl = Local System
remoteSystemLbl = Remote System

# Dialogs
deleteLocalFileTitle = Delete local file
deleteRemoteFileTitle = Delete remote file
deleteFileLbl = Are you sure you want to delete the selected file(s)?
createLocalDirTitle = Create local directory
createRemoteDirTitle = Create remote directory
createDirLbl = Enter directory name:
renameLocalFileTitle = Rename local file
renameRemoteFileTitle = Rename remote file
renameFileLbl = Enter new name:
transferStatusTitle = Transfer status
connectionFailedTitle = Connection failed
connectionDroppedTitle = Connection dropped
connectTitle = Connect
connectUsernameLbl = Username
connectPasswordLbl = Password
connectHostLbl = Host
connectTypeLbl = Connection type
connectAnonymousLbl = Anonymous
connectPassiveLbl = Passive
connectEnableProxyLbl = SOCKS proxy
connectProxyHostLbl = Host
browseVolumesLbl = Volumes
browseVolumesTitle = Browse local
browseDirsTitle = Browse remote
browseDirsLbl = Recent directories

keyTypeLbl = Key type
certTypeLbl = Certificate type
certVersionLbl = Version
certSerialLbl = Serial
certIssueLbl = Issuer
certSubjectLbl = Subject
certDateBeforeLbl = Valid not before
certDateAfterLbl = Valid not after
certSignatureLbl = Signature
certNameLbl = Name
certValueLbl = Value
acceptCertificateLbl = Accept certificate?
detailsCertificateLbl = Details

# Error Messages

errTitle = Error
errDeleteLbl = Unable to delete one or more file(s).
errRenameLbl = Unable to rename one or more file(s).
errSetDirLbl = Unable to access the specified directory.
errCreateDir = Unable to create the specified directory.
errConnection = Unable to connect to FTP server. Try again?
errConnectionDrop = Connection terminated.  Reconnect?
errLogin = Invalid FTP username or password.
errSshLogin = Invalid SSH username or passsword.
errSshVersion = Invalid SSH version on server.
errUploadLbl = Unable to upload one or more file(s).
errDownloadLbl = Unable to download one or more file(s).
errPermissions = Access denied.
errProxy = Unable to connect to proxy server.

# Table Columns
imageCol = 
fileNameCol = Name
sizeCol = Size
dateCol = Date





