<%@page contentType="text/html"%>
<%@page errorPage='/error.jsp'%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Tech View - FTP Upload</title>
<link rel="stylesheet" href="../../styles/globalheader_dk.css" type="text/css" charset="utf-8">
<link rel="stylesheet" href="../../styles/skins/flex_productbrowser.css" type="text/css" charset="utf-8">
<link rel="stylesheet" href="../../styles/skins/flex_menu.css" type="text/css" charset="utf-8">
<link rel="stylesheet" href="../../styles/skins/beige_medium.css" type="text/css" charset="utf-8">
<link rel="stylesheet" href="../../styles/text.css" type="text/css" charset="utf-8">
</head>
<body>
<div id="globalheader_background">
</div>
<div id="globalheader">
	<ul id="globalnav">
		<li id="blank"><a href="#"></a></li>
		<li id="blank"><a href="#"></a></li>
		<li id="blank"><a href="#"></a></li>
		<li id="blank"><a href="#"></a></li>
		<li id="blank"><a href="#"></a></li>
		<li id="blank"><a href="#"></a></li>
		<li id="blank"><a href="#"></a></li>
		<li id="home"><a href="#">Home</a></li>
	</ul>
</div>
<div class="mainwindow">
	<!-- CONTENT START      CONTENT START      CONTENT START      CONTENT START      CONTENT START      CONTENT START      CONTENT START      CONTENT START      CONTENT START      CONTENT START -->
	<div id="container960" class="layout1col_960">
		<div class="column">
			<div class="poster" id="border_dark">
				<div id="header_light">
					FTP LOGIN
				</div>
				<table width="958" border="0" align="left" cellpadding="0" cellspacing="0" bgcolor="#FFF">
					<tr>
						<td width="770"valign="top"><p>Type in some guidance and instructions to use the FTP interface here! <br>
								<a href="http://java.sun.com/getjava/">Java-Plugin version 1.4.2</a>.</p></td>
						<td align="right"><p><a href="http://java.sun.com/getjava/"><img src="get_java_blue-button.gif" width="88" height="31" border="0"></a></p></td>
					</tr>
					<tr>
						<td colspan="2" align="center"><object width="770" height="455" classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" codebase="http://java.sun.com/products/plugin/autodl/jinstall-1_4-windows-i586.cab#Version=1,4,2,0">
								<param name="code" value="com.jscape.ftpapplet.FtpApplet.class">
								<param name="archive" value="sftpapplet.jar">
								<param name="scriptable" value="false">
								<param name="params" value="parameters/uploadParams.txt">
								<comment>
									<embed 
												type="application/x-java-applet;version=1.4" \
												code="com.jscape.ftpapplet.FtpApplet.class" \
												archive="sftpapplet.jar" \
					
												params ="parameters/uploadParams.txt" \
					
												name="ftpapplet" \
												width="768" \
												height="455" \
												scriptable="false" \
												pluginspage = "http://java.sun.com/products/plugin/index.html#download">
									<noembed> </noembed>
									</embed>
								</comment>
							</object>
						</td>
					</tr>
					<!-- END SECURE FTP APPLET CODE -->
				</table>
			</div>
		</div>
	</div>
	<!-- CONTENT END      CONTENT END      CONTENT END      CONTENT END      CONTENT END      CONTENT END      CONTENT END      CONTENT END      CONTENT END      CONTENT END      CONTENT END -->
	<!-- FOOTER      FOOTER      FOOTER      FOOTER      FOOTER      FOOTER      FOOTER      FOOTER      FOOTER      FOOTER      FOOTER      FOOTER      FOOTER      FOOTER      FOOTER      FOOTER -->
	<div class="footer light">
		<div style="text-align:right; width: 660px; float:right;">
			Copyright 2007 &copy; Tech View A/S		</div>
	</div>
</div>
</body>
</html>



