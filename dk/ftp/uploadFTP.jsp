<%@page contentType="text/html"%>
<%@taglib prefix="techview" uri="/techview.tld"%>
<%@page errorPage='/error.jsp'%>
<%@page import="java.util.Locale"%>
<%@page import="com.techview.website.tagext.StringTag"%>
<%@taglib prefix="webforge" uri="/webforge.tld"%>
<%@page import='com.techview.website.browscap.Browscap'%>
<techview:controler />
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<HTML>
    <head> <head>
        <title><techview:string bundle='pageTitles' name='upload' /></title>
        <techview:pageid id="Upload" toolBarColor="grey"/>
        
        <style type="text/css" media="screen">
            body{
            z-index:100;
            width: 768px; 
            margin-top: 50;    
            margin-left: 250;
            }

            #test {
            margin-top: 50;    
            margin-left: 250;
            }
        </style>
        <link href='/styles/layout.css' rel='stylesheet' type='text/css'>
    </head>
    <BODY class='layout_background'> <!-- bgcolor="#CCCCCC" text="#000000" link="#FFFFFF" -->
        <div style="margin-left: 250px;">
            <table>
                <tr width='768' align='center'>
                    <div style="line-height: 50px;">
                        &nbsp;    
                    </div>
                </tr>
            </table>
            
            <div z-index: 99;>
                <jsp:include page='/tools/menu.jsp'/> <!-- tools/menu.jsp'/> -->
            </div>
            <table width="768" border="0" align="center" cellpadding="2" cellspacing="2" bgcolor="#ffffff">
                <tr>
                    <td width="768">
                        <!-- <font face="Arial, Helvetica, sans-serif" size="3"><b><font color="#FF3300" face="Trebuchet MS, Arial"><font size="-1">
                        </font></font></b></font> -->
                        <p align="left"><br>
                        <font face="Trebuchet MS, Arial" size="2"><br>
                        <techview:string bundle='ftp' name='accessInfo' /> <a href="http://java.sun.com/getjava/">Java-Plugin version 1.4.2</a>.</font></p>
                        <p align="left"><a href="http://java.sun.com/getjava/"><img src="get_java_blue-button.gif" width="88" height="31" border="0"></a>
                        </p>
                    </td>
                </tr>
            </table>
            <!-- BEGIN SECURE FTP CODE -->
            <CENTER>
                <object width="768" height="470" classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" codebase="http://java.sun.com/products/plugin/autodl/jinstall-1_4-windows-i586.cab#Version=1,4,2,0">
                    <param name="code" value="com.jscape.ftpapplet.FtpApplet.class">
                    <param name="archive" value="sftpapplet.jar">
                    <param name="scriptable" value="false">
                    <param name="params" value="parameters/uploadParams.txt"> 
                    <comment>
                        <embed 
                            type="application/x-java-applet;version=1.4" \
                            code="com.jscape.ftpapplet.FtpApplet.class" \
                            archive="sftpapplet.jar" \
                            params ="parameters/uploadParams.txt" \
                            name="ftpapplet" \
                            width="768" \
                            height="470" \
                            scriptable="false" \
                            pluginspage = "http://java.sun.com/products/plugin/index.html#download">
                            <noembed>            
                            </noembed>
                        </embed>
                    </comment>
                </object>
            </CENTER>
            <!-- END SECURE FTP APPLET CODE -->
        
            <table align='center' width="768" border='0' height='24'>
            <jsp:include page='/tools/footer.jsp'/>
            <!-- p align="left"><a href='http://tech-view.com/index.jsp'>Home</a></p -->
        </div>
    </BODY>	
</HTML>