<%@page contentType="text/html"%>
<%@taglib prefix="techview" uri="/techview.tld"%>
<%@page errorPage='/error.jsp'%>
<%@page import="java.util.Locale"%>
<%@page import="com.techview.website.tagext.StringTag"%>
<techview:controler/>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
    <!-- techview:pageid id="Contact" toolBarColor="Grey"/ -->
        <meta name="Keywords" content="<techview:string bundle='pageKeywords' name='home' />"/> 
        <meta name="Description" content="<techview:string bundle='pageDescription' name='home' />"/>         
        <jsp:include page='/tools/meta.jsp'/>
        <title>Tech View - <techview:string bundle='ftp' name='access' /></title>
    </head>
    <body class='layout_background font_default'>
    <div z-index: 99;>
            <jsp:include page='/tools/menu.jsp'/> <!-- tools/menu.jsp'/> -->
        </div>
        
        <table>
            <div z-index: 1;>
                <tr width='768' align='center'>
                    <div style="line-height: 23px;">
                        &nbsp;    
                    </div>
            
                </tr>
    
    <form name="jump">
<script>
function jumpMenu(){
location=document.jump.menu.options[document.jump.menu.selectedIndex].value;
}
</script>
<select  name="menu">
<option value="http://www.google.com">google</option>
<option value="http://www.krak.dk">krak</option>
<option value="http://www.msn.com">msn</option>
</select>
<a href="Javascript:jumpMenu()"><IMG SRC="http://media.tech-view.com/images/bgbank-onsite.jpg" border=0></a>
</form>
    
    
<noscript>
<a href='http://www.tech-view.com/ftp/accessFTP.jsp'>FTP Tech-View</a><br>
</noscript> 
        <!--jsp:include page='/tools/header.jsp'/-->
        
        <jsp:include page='/tools/topbar.jsp' />
        

        <tr>
            <td>
                <h1><techview:string bundle='ftp' name='access' /></h1>
            </td>
        </tr>
        <td>
            <p><techview:string bundle='ftp' name='connectDownload' />
        </td>
            <tr>
            <td>
            <a href='publicDownloadFTPinterface.jsp'><techview:string bundle='ftp' name='connect' /></a>
        </p>
                <br><br>
            </td>
        </tr>
        <tr>
            <td>
                <p><techview:string bundle='ftp' name='connectUpload' />
                    <a href='publicUploadFTPinterface.jsp'><techview:string bundle='ftp' name='connect' /></a>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <br>
                <br>
                <p align="left"><a href='http://tech-view.com/index.jsp'>Home</a></p>
            </td>
        </tr>
        </table>
        <table width: 85% border='0' height='24' class='layout_maintable' >
        <jsp:include page='/tools/footer.jsp'/>
    </body>
</html>



